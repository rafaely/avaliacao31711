#!/bin/bash 
choose_customization() {
    echo "Escolha uma opção de customização:"
    echo "1) Mudança de cor"
    echo "2) Exibir apenas o nome do usuário"
    echo "3) Prompt em duas linhas"
    echo "4) Personalização extra"
}

configure_prompt() {
    choose_customization
    read -p "Opção: " option

    case $option in
        1) 
            read -p "Escolha uma cor (30-37): " chosen_color
            bash apply.sh color $chosen_color
            ;;
        2) bash apply.sh username ;;
        3) bash apply.sh twolines ;;
        4) bash apply.sh extra ;;
        *) 
            echo "Opção inválida. Nenhuma customização aplicada."
            ;;
    esac
}

configure_prompt
