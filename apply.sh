#!bin/bash
apply_color() {
    local color=$1
    export PS1="\[\033[${color}m\]\u@\h \W\$ "
}

apply_username() {
    export PS1="\[\033[0m\]\u\$ "
}

apply_twolines() {
    export PS1="\[\033[0m\]\u@\h \W\n\$ "
}

apply_extra() {
    export PS1="\[\033[0m\]\u@\h \W [\$(date +%H:%M:%S)]\$ "
}

case $1 in
    color) apply_color $2 ;;
    username) apply_username ;;
    twolines) apply_twolines ;;
    extra) apply_extra ;;
    *) 
esac

echo "Customização aplicada!"

